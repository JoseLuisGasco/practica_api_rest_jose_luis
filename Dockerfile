# Imagen Raiz
FROM node

# Carpeta Raiz
WORKDIR /apitechu

# Copia de Archivos
ADD . /apitechu

# Añadir volumen
VOLUME ['/logs']

# Exponer puerto
EXPOSE 3000

# Instalar dependencias
# RUN npm install

# Comando de Inicialización
CMD ["npm", "start"]
