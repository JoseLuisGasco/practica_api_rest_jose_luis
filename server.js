var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var port = process.env.PORT || 3000;

var baseMlabURL = "https://api.mlab.com/api/1/databases/apitecujlgg/collections/";
var mLabAPIKey = "apiKey=E7qNG698tg90u0NLfeSbApQJH5mWRJmz";
var requestJson = require('request-json');

app.listen(port);
console.log("API escuchando en el puerto" + port);

app.get('/apitechu/v1',
  function(req, res) {
    console.log("GET /apitechu/v1");

    res.send(
      {
        "msg" : "Bienvenido a la API de Tech University"
      }
    )
  }
);

app.get('/apitechu/v1/users',
  function(req, res) {
    console.log("GET /apitechu/v1/users");
    res.sendFile('usuarios.json', {root: __dirname});
  }
);

app.get('/apitechu/v2/users',
  function(req, res) {
    console.log("GET /apitechu/v2/users");

    httpClient = requestJson.createClient(baseMlabURL);
    console.log("Cliente creado");

    httpClient.get("user?" + mLabAPIKey,
      function(err, resMLab, body) {
        var response = !err ? body : {
          "msg" : "error obteniendo usuarios."
        }
        res.send(response);
      }
    )
  }
);

app.get('/apitechu/v2/users/:id',
  function(req, res) {
    console.log("GET /apitechu/v2/users");

    var id = req.params.id;
    var query = 'q={"id" : ' + id + '}';

    httpClient = requestJson.createClient(baseMlabURL);
    console.log("Cliente creado");

    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
        if (err) {
        response = {
          "msg" : "Error obteniendo usuario."
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          response = body[0];
        } else {
          response = {
            "msg" : "Usuario no encontrado."
          };
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}
);


app.post('/apitechu/v1/users',
  function(req, res) {
    console.log("POST /apitechu/v1/users");
//  console.log(req);
    console.log("First_name is: " + req.body.first_name);
    console.log("Last_name is: " + req.body.last_name);
    console.log("Country_name is: " + req.body.country);

    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "country" : req.body.country
    };

    var users = require('./usuarios.json');
    users.push(newUser);
    writeUserDataToFile(users);

    res.send(users);
    /*res.send(
      {
      "msg" : "Usuario añadido con éxito"});*/

    console.log("Ususario añadido con éxito");
  }
);

app.delete("/apitechu/v1/users/:id",
  function(req, res){
    console.log("DELETE /apitechu/v1/users/:id");

    var users = require('./usuarios.json');
    users.splice(req.params.id -1, 1);
    writeUserDataToFile(users);

    res.send(
      {
        "msg" : "Usuario borrado con éxito"
      }
    );
  }
);

function writeUserDataToFile (data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile(
    "./usuarios.json",
    jsonUserData,
    "utf8",
    function(err) {
      if (err) {
        console.log(err);
      } else {
        console.log("Fichero de usuarios persistido");
      }
    }
  );
};

function writeUserDataToFile (data,filename) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile(
    filename,
    jsonUserData,
    "utf8",
    function(err) {
      if (err) {
        console.log(err);
      } else {
        console.log("Fichero de usuarios persistido");
      }
    }
  );
};
app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res) {
    console.log("Parámetros");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
);

app.post("/apitechu/v1/login",
  function(req, res) {
    console.log("POST /apitechu/v1/login");
//  console.log(req);
    console.log("email is: " + req.body.email);
    console.log("password is: " + req.body.password);

    var users_loggin = require('./usuarios_loggin.json');

    var encontrado = false;
    var userid;

    for (user of users_loggin) {
      console.log("user.email: " + user.email);
      console.log("req.body.email: " + req.body.email);
      console.log("user.password: " + user.password);
      console.log("req.body.password: " + req.body.password);
      if (user.email === req.body.email && user.password === req.body.password){
        console.log("email encontrado");
        user.logged = true;
        encontrado = true;
        userid = user.id;
        break;
      } else {
        console.log("email no encontrado");
        encontrado = false;
        }
    };

    if (encontrado) {
      writeUserDataToFile(users_loggin, './usuarios_loggin.json' );
      res.send(
        {
          "msg" : "Loggin Correcto" , "userID" : userid
        }
      );
    } else {
      res.send(
        {
          "msg" : "Loggin Incorrecto"
        }
      )
    }
    //users.push(newUser);
  //writeUserDataToFile(users);

  //res.send(users);
  /*res.send(
    {
    "msg" : "Usuario añadido con éxito"});*/

  console.log("Proceso loggin finalizado1");
  }
);

app.post("/apitechu/v1/logout",
  function(req, res) {
    console.log("POST /apitechu/v1/logout");
    console.log("Id is: " + req.body.id);

    var users_logout = require('./usuarios_loggin.json');

    var encontrado = false;
    var userid;

    for (user of users_logout) {

      console.log("user.id: " + user.id);
      console.log("req.body.id: " + req.body.id);
      if (user.id === req.body.id && user.logged){
        console.log("logout correcto");
        delete user.logged;
        encontrado = true;
        userid = user.id;
        break;
      } else {
        console.log("logout no correcto");
        encontrado = false;
        }

    };

    if (encontrado) {
      writeUserDataToFile(users_logout, './usuarios_loggin.json' );
      res.send(
        {
          "msg" : "Logout Correcto" , "userid" : userid
        }
      );
    } else {
      res.send(
        {
          "msg" : "Logout Incorrecto"
        }
      )
    }

  console.log("Proceso logout finalizado");
  }
);



app.post("/apitechu/v2/login",
  function(req, res) {
    console.log("POST /apitechu/v2/login");
    console.log("email is: " + req.body.email);
    console.log("password is: " + req.body.password);

    var email = req.body.email;
    var password = req.body.password;
    var query = 'q={"email":"' + email + '", "password":"' + password +'"}';

    httpClient = requestJson.createClient(baseMlabURL);
    console.log("cliente creado")

    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
        if (err) {
          response = {
            "msg" : "Error obteniendo usuario."
          }
          res.status(500);
        } else {
          if (body.length > 0) {
            var queryPut='q={"id":'+body[0].id+'}';
            var putBody ='{"$set":{"logged":true}}';
            response = body[0];
            console.log("user?"+queryPut+'&'+mLabAPIKey);
            console.log(putBody);
            httpClient.put("user?"+queryPut+'&'+mLabAPIKey, JSON.parse(putBody),
              function(errPut,resMlabPut,bodyPut){
                console.log('ERROR PUT:'+errPut);
                console.log('BODY PUT:'+bodyPut);
                res.send(response);
              }
            );
          } else {
              response = {
                "msg" : "Usuario no encontrado."
              };
              res.status(404);
            }
        }
        res.send(response);
      }
    )
  }
);

app.post("/apitechu/v2/logout",
  function(req, res) {
    console.log("POST /apitechu/v2/login");
    console.log("email is: " + req.body.email);
    console.log("password is: " + req.body.password);

    var email = req.body.email;
    var password = req.body.password;
    var query = 'q={"email":"' + email + '", "logged":true}';

    httpClient = requestJson.createClient(baseMlabURL);
    console.log("cliente creado")

    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
        if (err) {
          response = {
            "msg" : "Error obteniendo usuario."
          }
          res.status(500);
        } else {
          if (body.length > 0) {
            var queryPut='q={"id":'+body[0].id+'}';
            var putBody ='{"$unset":{"logged":""}}';
            response = body[0];
            console.log("user?"+queryPut+'&'+mLabAPIKey);
            console.log(putBody);
            httpClient.put("user?"+queryPut+'&'+mLabAPIKey, JSON.parse(putBody),
              function(errPut,resMlabPut,bodyPut){
                console.log('ERROR PUT:'+errPut);
                console.log('BODY PUT:'+bodyPut);
                res.send(response);
              }
            );
          } else {
              response = {
                "msg" : "Usuario no encontrado."
              };
              res.status(404);
            }
        }
        res.send(response);
      }
    )
  }
);


app.post("/apitechu/v2/altausuario",
  function(req, res) {
    console.log("POST /apitechu/v2/altausuario");
    console.log("email is: " + req.body.email);
    console.log("password is: " + req.body.password);
    console.log("First Name is: " + req.body.firstname);
    console.log("Last Name is: " + req.body.lastname);

    var email = req.body.email;
    var password = req.body.password;
    var firstname = req.body.firstname;
    var lastname = req.body.lastname;

    var querysort='s={"id":-1}';
    var queryvalidate='q={"email":"' + email + '"}';
    var querymaxid='q={}';

    console.log("query validar : " + queryvalidate);

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("user?"+ queryvalidate + "&" + mLabAPIKey,
      function(errvuser, resMLabvuser, bodyvuser) {
        if (errvuser) {
          response = {
            "msg" : "Error, el usuario existe",
            "body" : bodyvuser
          };
          res.status(500);
          res.send(response);
        } else {
            if (bodyvuser.length > 0) {
              response = {
                "msg" : "Error, el usuario existe",
                "body" : bodyvuser
              }
              res.status(500);
              res.send(response);
            } else {
                console.log("El usuario no existe");

                httpClient.get("user?"+ querymaxid + "&" + querysort + "&" + 'l=1' + "&" + mLabAPIKey,
                  function(errmaxid, resMLabmaxid, bodymaxid) {
                    console.log("error de recu. max id: " + errmaxid);
                    if (errmaxid) {
                      console.log("Respuesta NO ok");
                      response = {
                        "msg" : "Error insertando usuario."
                      };
                      res.status(500);
                      console.log("error de recu. max id: " + errmaxid);
                      res.send(response);
                    } else {
                        console.log("Respuesta ok");

                        if (bodymaxid.length>0) {
                          var newId = bodymaxid[0].id + 1;
                          console.log("newId: " + newId);
                          var insertBody = '{"id": '+ newId +' , "first_name":"' + firstname + '", "last_name":"' + lastname + '", "email":"' + email + '", "password":"' + password + '", "logged":true}';
                          console.log("insertbody: " + insertBody);

                          httpClient.post("user?" + mLabAPIKey, JSON.parse(insertBody),
                            function(errPost, resMLabPost, bodyPost) {
                              console.log("Respuesta de Insert Usuario");
                              console.log('ERROR POST:'+errPost);
                              console.log('BODY POST:'+bodyPost);
                              console.log("Final de proceso");
                              response = insertBody;
                              res.send(response);
                            }
                          )
                        } else {
                            response = {
                              "msg" : "Error recuperando max. id."
                            };
                            res.status(500);
                            res.send(response);
                          }
                      }
                  }
                )
              }
          }
      }
    )
  }
);

/* Consulta de las cuentas de un cliente*/
app.get('/apitechu/v2/users/:id/cuentas',
  function(req, res) {
    console.log("GET /apitechu/v2/users/:id/cuentas");

    var clienteid = req.params.id;
    var query = 'q={"clienteid" : ' + clienteid + '}';
    var querysort='s={"iban":1}';

    httpClient = requestJson.createClient(baseMlabURL);

    /*httpClient.get("cuentas?" + query + "&" + mLabAPIKey,*/
    httpClient.get("cuentas?"+ query + "&" + querysort + "&" + mLabAPIKey,
      function(err, resMLab, body) {
        if (err) {
        response = {
          "msg" : "Error obteniendo IBAN."
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          response = body;
        } else {
          response = {
            "msg" : "IBAN no encontrado."
          };
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}
);

/* Crear una cuentas de un cliente*/
app.post('/apitechu/v2/users/altacuenta',
  function(req, res) {
    console.log("POST /apitechu/v2/users/altacuenta");

    var querymaxcuentaid='q={}';
    var querysort = 's={"cuentaid":-1}';

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("cuentas?"+ querymaxcuentaid + "&" + querysort + "&" + 'l=1' + "&" + mLabAPIKey,
      function(errmaxcuentaid, resMLabmaxcuentaid, bodymaxcuentaid) {

        if (errmaxcuentaid) {
          console.log("Respuesta max cuentaid NO ok");
          response = {
            "msg" : "Error insertando cuenta."
          };
          res.status(500);
          res.send(response);
        } else {
            if (bodymaxcuentaid.length>0) {

              var clienteid = req.body.clienteid;

              var date = new Date();
              var d = date.getDate();
              var dia = (d < 10) ? '0' + d : d;
              var m = date.getMonth() + 1;
              var mes = (m < 10) ? '0' + m : m;
              var h = date.getHours();
              var hora = (h < 10) ? '0' + h : h;
              var mto = date.getMinutes();
              var minuto = (mto < 10) ? '0' + mto : mto;

              var newiban = "BA01 0001 7999 " + date.getFullYear() + " " + mes + dia + " " + hora + minuto;
              console.log("New iban: " + newiban);
              console.log("Cliente id entrada: " + clienteid)

              var newCuentaId = bodymaxcuentaid[0].cuentaid + 1;
              console.log("newId: " + newCuentaId);
              var insertCuenta = '{"iban":"' + newiban + '", "cuentaid":' + newCuentaId + ', "clienteid":' + clienteid + ', "saldo":0}';
              console.log("insertCuenta: " + insertCuenta);

              httpClient.post("cuentas?" + mLabAPIKey, JSON.parse(insertCuenta),
                function(errPostCuenta, resMLabPostCuenta, bodyPostCuenta) {
                  console.log("Respuesta de Insert Usuario");
                  console.log('ERROR POST:'+errPostCuenta);
                  console.log('BODY POST:'+bodyPostCuenta);
                  console.log("Final de proceso");
                  response = insertCuenta;
                  res.send(response);
                }
              )
            }
          }
      }
    )
  }
);

/* Consulta de los movimientos de una cuenta de un cliente*/
app.get('/apitechu/v2/cuentas/:cuentaid/movimientos',
  function(req, res) {
    console.log("GET /apitechu/v2/cuentas/:cuentaid/movimientos");

    var cuentaid = req.params.cuentaid;
    var query = 'q={"cuentaid" : ' + cuentaid + '}';
    var querysort='s={"fecha":1}';

    httpClient = requestJson.createClient(baseMlabURL);

    /*httpClient.get("cuentas?" + query + "&" + mLabAPIKey,*/
    httpClient.get("movimientos?"+ query + "&" + querysort + "&" + mLabAPIKey,
      function(errmov, resMLabmov, bodymov) {
        if (errmov) {
          response = {
            "msg" : "Error obteniendo IBAN."
          }
          res.status(500);
        } else {
            if (bodymov.length > 0) {
              console.log("bodymov: " + bodymov)
              response = bodymov;
            } else {
                response = {
                  "msg" : "IBAN no encontrado."
                };
                res.status(404);
              }
          }
        res.send(response);
      }
    )
  }
);
